<chapter id="chapter-sample">
  <title>Sample Chapter</title>

  <para>
     Lorem ipsum dolor sit amet. This is sample body text. This is
     where you type the content of your adventure. You can use <emphasis>italics</emphasis> and
     <emphasis role="bold">bold</emphasis> text.
  </para>

  <para>
    Use &#60;emphasis&#62; for <emphasis>italics</emphasis>.
  </para>

  <para>
    Use &#60;emphasis role=&#34;bold&#34;&#62; for <emphasis
    role="bold">inline bold</emphasis>.
  </para>

  <section id="section-sample">
    <title>Sample Section</title>

    <para>
      Use the &#60;note&#62; tag to create &#35;d9d9d9 highlighted
      boxes so the DM reading your adventure knows what to read aloud,
      just like in an official campaign.
    </para>

    <!-- use <note> for highlighted sections -->

    <note id="read-intro">
      <title>Read this aloud to your players&#58;</title>
      <para>
	Litora massa sed, a tellus sociis mauris, pharetra tempor
	odio. Ut interdum egestas fusce ut nulla, fermentum lacinia
	lorem nostra orci, nunc nisi felis suspendisse id leo,
	vehicula libero commodo congjue porta nunc mauris, amet
	nulla. Enim blandit pretium. Scelerisque erat nunc euismod nec
	diam turpis, ullamcorper amet, vitae ullamcorper cursus, urna
	risus dictumst suscipit.
      </para>
    </note>
    
  <para>
    Massa purus nulla eu sapien varius, amet feugiat integer nulla
    sagittis mattis, orci quisque sit magna eros eu feugiat, nullam
    dignissim quis nulla diam nisl, in euismod. Sagittis rhoncus
    tempus in, pellentesque totam aliquam. Pellentesque consequat nibh
    sed aliquam donec.
  </para>

  <para>
    Arcu vulputate, ullamcorper orci, quis vitae
    blandit, pellentesque vestibulum lorem scelerisque, pellentesque
    diam ut vulputate mauris enim ligula. Nascetur suspendisse justo
    ultrices maecenas sodales, nec lacus dui, duis lorem, id ac enim
    vestibulum urna.
  </para>

  <important id="sidebar"> 
    <title>Sidebar example</title>
    <para>
      Use the &#60;important&#62; tag to emphasize content. This is
      the &#34;sidebar&#34; element in the official template. 
    </para>
  </important>

  <para>
    Aliquam ante arcu vel torquent augue consequat,
    pellentesque faucibus vitae egestas, luctus urna sodales
    non. Turpis sapien est enim felis at vel, eget morbi nec molestie
    pharetra, nibh orci erat integer eros condimentum. Leo libero
    magna neque mollis non, vestibulum in nam, vitae vitae maecenas
    erat erat non quis, in et fermentum amet metus, ligula lectus.
  </para>
  
  <para>
    Use the custom rule &#60;?hard-pagebreak?&#62; to force page
    breaks. Use these sparingly and only when you're really
    really sure that you aren't going to add any more text. If you
    insert them during development, every time you make an edit,
    you'll probably have to adjust your forced page breaks, which
    may drive you legally insane.
  </para>

  <!-- full page images must be isolated by page breaks or else
  they bleed into the adjacent column (or off the page). -->

  <?hard-pagebreak?>

    <informalfigure pgwide="1">
      <mediaobject>
	<imageobject>
	  <imagedata fileref="../img/diceydungeon.jpeg" width="7in" format="jpeg" />
	</imageobject>
      </mediaobject>
    </informalfigure>

    <?hard-pagebreak?>
    
  <!-- the <bridgehead> element can be used in the body as small 
       section headings that don't register in table of contents -->

  <bridgehead>This is a bridgehead.</bridgehead>

  <para>
    This section is not a real section. It&#39;s just been set apart
    by a &#60;bridgehead&#62;. A bridgehead renders a title but does
    not add the title to the table of contents. Sometimes that's useful.
  </para>
  
  <para>
    Donec vel mi. Per irure tellus magna, ultrices curabitur et semper
    nisl in dapibus, egestas malesuada nec tortor posuere in montes,
    viverra pretium. Pretium suspendisse, neque nam amet lorem
    rhoncus, tristique dignissim curabitur fusce nisl ut at, morbi
    nulla ac cras egestas eget. Dolor enim, fames ac, mauris luctus
    fuga ipsum nonummy sed.
  </para>

  <!-- instead of bridgehead, you can use strong. -->
  
  <para><emphasis role="strong">This is &#60;emphasis role=&#34;strong&#34;&#62; text.</emphasis></para>

  <para>
    Alternately, you can use &#60;para&#62;&#60;emphasis
    role=&#34;strong&#34;&#62;strong&#60;&#47;para&#62;&#60;&#47;emphasis&#62;
    text. This tag has been repurposed in the style mods to render as
    bold, and to have a forced line break after it. In other words,
    it's a &#34;soft&#34; heading, which you might use in stat blocks,
    or just to highlight important breaks in the flow of the game.
  </para>
  
  <para>
    A malesuada a, tincidunt sit possimus
    vestibulum ultricies et, vel penatibus pulvinar augue
    faucibus. Nunc nullam molestie libero et arcu, mi nulla
    porta. Arcu magna nec, tincidunt fermentum tortor urna quis donec
    ut, dolor adipiscing consectetuer, id et id faucibus rhoncus
    posuere in, imperdiet curabitur sed elit. Per vitae facilisis, at
    qui aenean volutpat nunc amet, sit justo amet, tempora nam est,
    lacinia dis duis nulla. Ante fusce, elit eget velit, elit vel
    dignissim magna in.
  </para>

  <para>
    At this time, stat blocks are incomplete in this template. A quick
    and easy workaround is to use &#60;tip&#62; tags. Prettier stat
    blocks might be something I work on later, but since they usually
    involve tables, they tend to be space-inefficient, so it&#39;s not
    a priority.
  </para>
  
  <!-- if you need to pad a column with whitespace to avoid
  weird breaks, use literallayout and a bunch of blank lines -->

  <literallayout>



  </literallayout>

  <!-- use the tip tag for stat blocks -->
  
  <tip id="stat-block">
      <title>Stat block</title>
      <para>XP 200 &#40;CR 1/2&#41;</para>
      <para>NE Medium undead &#124; Init +0; Senses darkvision 60 ft.; Perception +0</para>

      <para><emphasis role="bold">DEFENSE</emphasis></para>
      <para>AC 12, touch 10, flat-footed 12 (+2 natural)</para>
      <para>hp 12 (2d8+3) &#124; Fort +0, Ref +0, Will +3</para>
      <para>DR 5/slashing; Immune undead traits</para>

      <para><emphasis role="bold">OFFENSE</emphasis></para>
      <para>Speed 30 ft. &#124; Melee slam +4 (1d6+4)</para>

      <para><emphasis role="bold">STATISTICS</emphasis></para>
      <para>Str 17, Dex 10, Con —, Int —, Wis 10, Cha 10 &#124; Base Atk +1; CMB +4; CMD 14</para>
      <para>Feats&#58; Toughness &#124; Special quality&#58;  staggered</para>

      <para>Animated corpses of dead creatures, forced into foul
      unlife by necromantic magic, like <emphasis>animate
      dead</emphasis>. While the most commonly encountered zombies are
      slow and tough, some are faster and more strategic.
      </para>
      <para>Zombies attack until destroyed, having no regard for their
      own safety.
      </para>
      <para>Although capable of following orders, zombies are more
      often unleashed into an area with no command other than to kill
      living creatures.
      </para>
    </tip>      
    
  <para>
    Commodi aliquam proin magnis eu, eu leo, mus lobortis, nulla
    venenatis. Adipiscing dapibus. Aptent congue urna nam donec arcu
    est, consequat nunc posuere sit, sem suscipit augue, fugit tempus
    volutpat. Vel tortor, maecenas ligula dictum. A turpis diam at,
    vivamus tempus cras rhoncus nostra semper sed. Curae aliquet
    aut. Ipsum eget, nulla non, nam enim pharetra placerat lacinia,
    risus urna eget adipiscing elementum penatibus rutrum, purus
    curabitur nec arcu enim enim. Vestibulum aenean diam, nec id a
    placerat nulla illum. Doloribus nihil auctor et rutrum. Est nam
    non omnis nonummy, fringilla aenean, lectus vel ullamcorper proin,
    eu neque. Et nunc et minus ridiculus rutrum. Pede curabitur
    molestie, consectetuer orci, in elit ipsum.
  </para>

  <para>
    Once you&#39;re finished writing, build your finished work with
    the GNUmakefile included in this repository.
  </para>
  
  </section>
</chapter>
